const express = require('express');
const app = express();
const port = 8082;

// Define routes
app.get('/', (req, res) => {
    res.send('Hello, World! from Hameed Akshal.');
});

app.get('/rifath', (req, res) => {
    res.send('this is rifath');
});

app.get('/fahim', (req, res) => {
    res.send('this is fahim.');
});

// Start the server
app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});
